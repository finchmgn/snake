package game.snake;

public enum SnakeDirection {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NOTHING
}
