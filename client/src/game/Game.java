package game;

import game.graphics.Graphics;
import game.graphics.State;
import game.snake.Snake;
import game.snake.SnakeDirection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Game class combines fields and methods
 * responsible for mechanics of the game
 *
 * Implements KeyListener interface for catching player keystrokes
 */
public class Game implements KeyListener {
    /**
     * Grid size on which the snake crawls.
     * Grid is always square, so we specify only one value for the dimension
     * Measured in conditional cells (units)
     */
    public static final int GRID_SIZE = 20;

    /** Size of one cell in the grid. Measured in pixels */
    public static final int GRID_CELL_SIZE = 20;

    /**
     * Game window size.
     * Like the grid, the window is always square, so we specify only one value for the dimension
     */
    public static final int GAME_FIELD_SIZE = GRID_SIZE * GRID_CELL_SIZE;

    /** The snake that the player will control  */
    private final Snake snake;

    /** Prey that the snake will catch */
    private final Prey prey;

    /** Graphics */
    private final Graphics graphics;

    /** Game class constructor */
    public Game() {
        snake = new Snake();
        prey = new Prey(snake);
        graphics = new Graphics(this);

        createGameWindow(graphics);
    }

    public Snake getSnake() {
        return snake;
    }

    public Prey getPrey() {
        return prey;
    }

    public void start() {
        graphics.state = State.RUNNING;
    }

    /**
     * Creates game window
     * @param graphics graphics components
     */
    private void createGameWindow(Graphics graphics) {
        /* Creates JFrame and JPanel instances for game window and field */
        JFrame jFrame = new JFrame();
        JPanel jPanel = new JPanel();

        /* Sets game window size based on field size */
        jPanel.setPreferredSize(new Dimension(GAME_FIELD_SIZE, GAME_FIELD_SIZE));
        jFrame.getContentPane().add(jPanel);
        jFrame.pack();

        /* Sets game window title */
        jFrame.setTitle("Snake");

        /* Sets operation on window close action. A constant is passed as an argument,
        which says that the application needs to be closed */
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        /* Disables window resizing (stretching) */
        jFrame.setResizable(false);

        /* Specifies the position of the current window relative to the parent window.
        The null value allows you to set the current window to the center */
        jFrame.setLocationRelativeTo(null);

        /* Add graphics components to window */
        jFrame.add(graphics);

        /* Specifies that the window will be visible on the screen.
        If set to false, the window will exist in memory, but will not be displayed on the screen. */
        jFrame.setVisible(true);
    }

    private boolean checkWallCollision() {
        return snake.getX() < 0 || snake.getX() >= GAME_FIELD_SIZE
                || snake.getY() < 0 || snake.getY() >= GAME_FIELD_SIZE;
    }

    private boolean checkPreyCollision() {
        return snake.getX() == prey.getX()
                && snake.getY() == prey.getY();
    }

    private boolean checkSnakeCollision() {
        for (int i = 1; i < snake.getBody().size(); i++) {
            if (snake.getX() == snake.getBody().get(i).x
                    && snake.getY() == snake.getBody().get(i).y) {
                return true;
            }
        }

        return false;
    }

    public void update() {
        if (!graphics.state.equals(State.RUNNING)) {
            return;
        }

        if (checkPreyCollision()) {
            snake.grow();
            prey.randomSpawn(snake);
            return;
        }

        if (checkWallCollision() || checkSnakeCollision()) {
            graphics.state = State.END;
            return;
        }

        snake.move();
    }

    /** KeyListener methods */
    @Override
    public void keyTyped(KeyEvent e) {}

    /** Catching key pressed event */
    @Override
    public void keyPressed(KeyEvent e) {
        /* Gets code of pressed key */
        int keyCode = e.getKeyCode();

        if (graphics.state.equals(State.RUNNING)) {
            /* Snake crawl directions */
            switch (keyCode) {
                case KeyEvent.VK_W -> snake.setMove(SnakeDirection.UP);
                case KeyEvent.VK_S -> snake.setMove(SnakeDirection.DOWN);
                case KeyEvent.VK_A -> snake.setMove(SnakeDirection.LEFT);
                case KeyEvent.VK_D -> snake.setMove(SnakeDirection.RIGHT);
            }
        } else {
            this.start();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {}
}
