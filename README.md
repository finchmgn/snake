# Snake

Simple game. Created to learn the basics of Java. <br>
This is my first Java app

__The code is deliberately left with obvious comments to better understand each action.__

Based on a video tutorial <br>
But some changes have been made:
- Minor logic changes
- Fixed bug with window size
- Refactored
- Added comments

Video tutorial link: https://www.youtube.com/watch?v=l2KZsvaMeLs <br>
Tutorial author: CodingWithTim <br>
Tutorial author link: https://www.youtube.com/c/CodingWithTim
<br>

#### Assets
1) Grass texture downloaded from https://www.artstation.com/artwork/q9eEnP <br>
Author: Myrthe van Tol <br>
Author link: https://www.artstation.com/myrthevantol

2) Prey icons downloaded from https://www.flaticon.com/packs/animal-2 <br>
Author: Freepik <br>
Author link: https://www.freepik.com