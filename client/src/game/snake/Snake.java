package game.snake;

import game.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/** The snake that the player will control  */
public class Snake {
    /**
     * Snake body is consists of blocks.
     * Array of squares from the AWT library is used to simulate it.
     */
    private final ArrayList<Rectangle> body;

    /** Snake block size */
    private final int blockSize = Game.GRID_CELL_SIZE;

    /** Snake move direction */
    private SnakeDirection move;

    public Snake() {
        move = SnakeDirection.NOTHING;
        body = new ArrayList<>();

        /* Creates initial snake body */
        int initialSize = 3;
        for (int i = 0; i < initialSize; i++) {
            /* Compute block coordinates */
            Map<Character, Integer> coordinates = new HashMap<>();
            coordinates.put('x', (Game.GRID_SIZE / 2 - i) * blockSize);
            coordinates.put('y', Game.GRID_SIZE / 2 * blockSize);

            /* Creates and adds block to body */
            Rectangle snakeBlock = new Rectangle(blockSize, blockSize);
            snakeBlock.setLocation(coordinates.get('x'), coordinates.get('y'));
            body.add(snakeBlock);
        }
    }

    public int getX() { return body.get(0).x; }

    public int getY() { return body.get(0).y; }

    /** Snake body getter */
    public ArrayList<Rectangle> getBody() {
        return body;
    }

    /** Gets inverted direction value */
    private SnakeDirection getInvertDirection(SnakeDirection direction) {
        SnakeDirection invert = null;

        switch (direction) {
            case UP -> invert = SnakeDirection.DOWN;
            case DOWN -> invert = SnakeDirection.UP;
            case LEFT -> invert = SnakeDirection.RIGHT;
            case RIGHT -> invert = SnakeDirection.LEFT;
        }

        return invert;
    }

    public void setMove(SnakeDirection direction) {
        if (!move.equals(getInvertDirection(direction))) {
            move = direction;
        }
    }

    public void move() {
        if (move.equals(SnakeDirection.NOTHING)) {
           return;
        }

        grow();
        body.remove(body.size() - 1);
    }

    public void grow() {
        Rectangle head = body.get(0);
        Rectangle newBlock = new Rectangle(blockSize, blockSize);

        switch (move) {
            case UP -> newBlock.setLocation(head.x, head.y - blockSize);
            case DOWN -> newBlock.setLocation(head.x, head.y + blockSize);
            case LEFT -> newBlock.setLocation(head.x - blockSize, head.y);
            case RIGHT -> newBlock.setLocation(head.x + blockSize, head.y);
        }

        body.add(0, newBlock);
    }
}
