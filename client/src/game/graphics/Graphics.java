package game.graphics;

import game.Game;
import game.Prey;
import game.snake.Snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class responsible for graphic effects and game rendering
 * Extends from JPanel to be able to apply JFrame methods for this class
 * Implements ActionListener interface for catching actions
 */
public class Graphics extends JPanel implements ActionListener {

    /**
     * State of game. Starting screen, game process or results screen
     */
    public State state;

    private final Snake snake;
    private final Prey prey;
    private final Game game;

    private final int xLegendPos;
    private final int yLegendPos;

    public final Image backgroundImage;

    /**
     * Graphics class constructor
     */
    public Graphics(Game game) {
        this.game = game;
        this.snake = game.getSnake();
        this.prey = game.getPrey();

        xLegendPos = Game.GAME_FIELD_SIZE / 2;
        yLegendPos = Game.GAME_FIELD_SIZE / 2;

        backgroundImage = new ImageIcon("client/src/assets/images/grass-600-min.jpg").getImage();

        /* Sets game state to "START" */
        state = State.START;

        /* Adds key listener */
        this.addKeyListener(game);
        this.setFocusable(true);

        /* Timer which render game */
        Timer timer = new Timer(100, this);
        timer.start();
    }

    public void paint(java.awt.Graphics graphics) {
        super.paint(graphics);

        /* Cast graphics to Graphics2D */
        Graphics2D g2d = (Graphics2D) graphics;

        /* Game field background */
        /*
        It's for displaying rectangle instead background image
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, Game.GAME_FIELD_SIZE, Game.GAME_FIELD_SIZE);
        */

        g2d.drawImage(backgroundImage, 0, 0, this);

        switch (state) {
            case START -> {
                drawLegendString(g2d, "PRESS ANY KEY");
            }
            case RUNNING -> {
                /* Prey */
                /*
                It's for displaying rectangle instead icon
                g2d.setColor(Color.RED);
                g2d.fillRect(prey.getX(), prey.getY(), Game.GRID_CELL_SIZE, Game.GRID_CELL_SIZE);
                */

                Image preyIcon = new ImageIcon(prey.getIcon()).getImage();

                /* size of the cell and the icon may differ, so you need to set the offset for the icon (only for display) */
                int offsetX = (preyIcon.getWidth(this) - Game.GRID_CELL_SIZE) / 2;
                int offsetY = (preyIcon.getHeight(this) - Game.GRID_CELL_SIZE) / 2;

                g2d.drawImage(preyIcon, prey.getX() - offsetX, prey.getY() - offsetY, this);

                /* Snake */
                g2d.setColor(new Color(75, 75, 75));

                for (Rectangle snakeBlock : snake.getBody()) {
                    g2d.fill(snakeBlock);
                }
            }
            case END -> {
                /* Score computes  */
                String score = String.valueOf(snake.getBody().size() - 3);
                drawLegendString(g2d, "YOUR SCORE: " + score);
            }
        }
    }

    private void drawLegendString(Graphics2D g2d, String legend) {
        int width = g2d.getFontMetrics().stringWidth(legend);
        g2d.setColor(Color.WHITE);
        g2d.drawString(legend, xLegendPos - width / 2, yLegendPos);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
        game.update();
    }

}
