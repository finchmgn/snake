package game;

import game.snake.Snake;

import java.awt.*;
import java.util.Random;

/**
 * Prey class. Snake crawls and eats her prey
 */
public class Prey {

    /** Prey horizontal coordinate */
    private int x;

    /** Prey vertical coordinate */
    private int y;

    private final Random random;

    private String icon;

    /** Class constructor */
    public Prey(Snake snake) {
        random = new Random();
        randomSpawn(snake);
    }

    /**
     * Prey coordinates getters
     * @return coordinate
     */
    public int getX() { return x; }
    public int getY() { return y; }

    public String getIcon() { return icon; }

    /** Gets random prey icon path */
    public String getRandomIcon() {
        String[] icons = new String[]{
                "cat.png",
                "crow.png",
                "hen.png",
                "rabbit.png",
                "raccoon.png"
        };

        int index = random.nextInt(4);

        return "client/src/assets/images/prey/" + icons[index];
    }

    /**
     * Generates random coordinates to spawn prey
     * */
    public void randomSpawn(Snake snake) {
        icon = getRandomIcon();

        boolean onSnake = true;

        while(onSnake) {
            onSnake = false;

            x = getRandomCoordinate();
            y = getRandomCoordinate();

            for(Rectangle block : snake.getBody()) {
                if (block.x != x || block.y != y) continue;

                onSnake = true;
                break;
            }
        }
    }

    /**
     * Generates random coordinate
     * @return random grid coordinate
     */
    private int getRandomCoordinate() {
        return random.nextInt(Game.GRID_SIZE - 1) * Game.GRID_CELL_SIZE;
    }
}
